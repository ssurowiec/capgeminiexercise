import { CustomerService } from './../../../Services/customer.service';
import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerDetailsDto } from 'src/app/Models/CustomerDetailsDto';

@Component({
    selector: 'app-customer',
    templateUrl: 'customer.component.html',
    styleUrls: ['customer.component.css']
})

export class CustomerComponent implements OnInit {
    @Input() private id: string;
    public isEditable: boolean = false;
    public customer: Observable<CustomerDetailsDto>;
    constructor(private customerService: CustomerService) { }

    ngOnInit() {
        this.customer = this.customerService.Customer(this.id);
     }

}