import { CustomerService } from './../../Services/customer.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerDto } from 'src/app/Models/CustomerDto';

@Component({
    selector: 'app-customers',
    templateUrl: 'customers.component.html',
    styleUrls: ['customers.component.css']
})

export class CustomersComponent implements OnInit {
    public customers: Array<CustomerDto> = [];
    public customersState: Map<string,boolean>;
    constructor(private customerService: CustomerService) { }

    ngOnInit() {
        this.customerService.Customers().subscribe((customers: Array<CustomerDto>) => {
            this.customers = new Array(...customers);
        });
     }
     public OnDelete(customer: CustomerDto) {
        this.customerService.Delete(customer.id).subscribe(()=>{
            console.log('deleted');
        });
        this.customers.splice(this.customers.indexOf(customer), 1);
     }
}