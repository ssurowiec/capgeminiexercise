export interface CustomerDto {
    id: string,
    firstName: string,
    lastName: string,
    detailEnabled: boolean
}