export interface CustomerDetailsDto {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    city: string,
    street: string,
    postalCode: string
}