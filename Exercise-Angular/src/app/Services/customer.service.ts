import { CustomerDetailsDto } from './../Models/CustomerDetailsDto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { CustomerDto } from '../Models/CustomerDto';

@Injectable()
export class CustomerService {
    constructor(private httpClient: HttpClient) { }
    
    public Customers(): Observable<Array<CustomerDto>> {
        return this.httpClient.get<Array<CustomerDto>>('http://localhost:5000/api/customers')
            .pipe(map((customers, index) => {
                if(customers.length > 0) {
                    customers[index].detailEnabled = false;
                }
                return customers;
            }));
    }
    public Customer(id: string): Observable<CustomerDetailsDto> {
       return this.httpClient.get<CustomerDetailsDto>(`http://localhost:5000/api/customers/${id}`);
    }
    public Delete(customerId: string) {
        return this.httpClient.delete(`http://localhost:5000/api/customers/${customerId}`);
    }
}