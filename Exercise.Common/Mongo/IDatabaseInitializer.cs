﻿namespace Exercise.Common.Mongo
{
    public interface IDatabaseInitializer
    {
        void Initialize();
    }
}