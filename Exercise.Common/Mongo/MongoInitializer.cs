﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;

namespace Exercise.Common.Mongo
{
    public class MongoInitializer : IDatabaseInitializer
    {
        private bool _initialized;

        public MongoInitializer()
        {
        }
        public void Initialize()
        {
            if (_initialized)
            {
                return;
            }
            RegistryConventions();
            _initialized = true;
        }
        private void RegistryConventions()
        {
            ConventionRegistry.Register("Conventions", new MongoConvention(), x => true);
        }
        private class MongoConvention : IConventionPack
        {
            public IEnumerable<IConvention> Conventions =>
                new List<IConvention>
                {
                    new IgnoreExtraElementsConvention(true),
                    new EnumRepresentationConvention(BsonType.String),
                    new CamelCaseElementNameConvention()
                };
        }
    }
}