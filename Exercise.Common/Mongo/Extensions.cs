using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Exercise.Common.Mongo
{
    public static class Extensions
    {
        public static void AddMongo(this IServiceCollection services, IConfiguration congifuration)
        {
            var options = new MongoOptions();
            var section = congifuration.GetSection("mongo");
            section.Bind(options);
            services.AddSingleton<MongoClient>(c =>
            {
                return new MongoClient(options.ConnectionString);
            });
            services.AddTransient<IMongoDatabase>(cfg =>
            {
                var client = cfg.GetService<MongoClient>();
                return client.GetDatabase(options.DatabaseName);
            });
            services.AddTransient<IDatabaseInitializer, MongoInitializer>();
        }
    }
}