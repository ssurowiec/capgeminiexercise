﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Common.AutoMapper
{
    public interface IAutoMapperConfiguration
    {
        IMapper GenerateMaps();
    }
}
