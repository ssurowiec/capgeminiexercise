﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Common.AutoMapper
{
    public static class Extensions
    {
        public static void AddAutoMapper<T>(this IServiceCollection services) where T : IAutoMapperConfiguration
        {
            services.AddTransient(typeof(IAutoMapperConfiguration),typeof(T));
            services.AddSingleton<IMapper>(cfg =>
            {
                var service = cfg.GetService<IAutoMapperConfiguration>();
                return service.GenerateMaps();
            });
        }
    }
}
