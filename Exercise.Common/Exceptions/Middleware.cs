﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Common.Exceptions
{
    public class Middleware
    {
        private readonly RequestDelegate _request;
        public Middleware(RequestDelegate request)
        {
            _request = request;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _request.Invoke(context);
            }
            catch(ExerciseException ex)
            {
                var result = JsonConvert.SerializeObject(new
                {
                    error = ex.Reason,
                    code = ex.Code
                }
                );
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(result);
            }
        }
    }
}
