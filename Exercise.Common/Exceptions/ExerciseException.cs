﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Common.Exceptions
{
    public class ExerciseException: Exception
    {
        public string Reason { get; }
        public string Code { get; }
        public ExerciseException(string reason, string code)
        {
            Reason = reason;
            Code = code;
        }
    }
}
