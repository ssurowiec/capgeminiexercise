﻿using Exercise.Api.Domain.Models;
using Exercise.Api.Domain.Repository;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IMongoDatabase _database;

        public CustomerRepository(IMongoDatabase database)
        {
            _database = database;
        }
        public Task AddAsync(Guid id,string firstName, string lastName, string email, string street, string postalCode, string city)
        {
            var address = new Address(street, city, postalCode);
            var customer = new Customer(id, firstName, lastName, email, address);
            return Collection.InsertOneAsync(customer);
        }

        public Task<List<Customer>> BrowseAsync()
            => Collection.AsQueryable().ToListAsync();

        public Task<Customer> GetAsync(Guid customerId)
            => Collection.AsQueryable().FirstOrDefaultAsync(c => c.Id == customerId);
        public Task<Customer> GetAsync(string email)
            => Collection.AsQueryable().FirstOrDefaultAsync(c => c.Email == email);

        public Task RemoveAsync(Guid customerId)
            => Collection.DeleteOneAsync(c => c.Id == customerId);

        public async Task UpdateAsync(Guid customerId, string firstName, string lastName, string email, string street, string postalCode, string city)
        {
            var customer = await GetAsync(customerId);
            customer.SetFirstName(firstName);
            customer.SetLastName(lastName);
            customer.SetEmail(email);
            customer.Address.SetCity(city);
            customer.Address.SetStreet(street);
            customer.Address.SetPostalCode(postalCode);
            await Collection.ReplaceOneAsync(c => c.Id == customerId, customer);
        }
        private IMongoCollection<Customer> Collection => _database.GetCollection<Customer>("Customers");
    }
}
