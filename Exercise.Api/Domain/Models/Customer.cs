﻿using Exercise.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Exercise.Api.Domain.Models
{
    public class Customer
    {
        private readonly Regex _emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        public Guid Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public Address Address { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Customer(Guid id, string firstName, string lastName, string email, Address address)
        {
            Id = id;
            SetFirstName(firstName);
            SetLastName(lastName);
            SetEmail(email);
            SetAddress(address);
            CreatedAt = DateTime.UtcNow;
        }
        private Customer()
        {

        }
        public void SetFirstName(string firstName)
        {
            if(string.IsNullOrWhiteSpace(firstName))
            {
                throw new ExerciseException($"{nameof(firstName)} can't be null","101");
            }
            FirstName = firstName;
            Update();
        }
        public void SetLastName(string lastName)
        {
            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new ExerciseException($"{nameof(lastName)} can't be null", "101");
            }
            LastName = lastName;
            Update();
        }
        public void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ExerciseException($"{nameof(email)} can't be null", "101");
            }
            if (!_emailRegex.IsMatch(email))
            {
                throw new ExerciseException($"{nameof(email)} is not valid", "101");
            }
            Email = email;
            Update();
        }
        public void SetAddress(Address address)
        {
            if(address is null)
            {
                throw new Exception($"{nameof(address)} can't be null");
            }
            Address = address;
            Update();
        }
        private void Update()
        {
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
