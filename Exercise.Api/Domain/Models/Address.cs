﻿using Exercise.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Domain.Models
{
    public class Address
    {
        public string Street { get; private set; }
        public string City { get; private set; }
        public string PostalCode { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Address(string street, string city, string postalCode)
        {
            SetStreet(street);
            SetCity(city);
            SetPostalCode(postalCode);
        }
        private Address() { }
        public void SetStreet(string street)
        {
            if (string.IsNullOrWhiteSpace(street))
            {
                throw new ExerciseException($"{nameof(street)} can't be null", "101");
            }
            Street = street;
            Update();
        }
        public void SetCity(string city)
        {
            if (string.IsNullOrWhiteSpace(city))
            {
                throw new ExerciseException($"{nameof(city)} can't be null", "101");
            }
            City = city;
            Update();
        }
        public void SetPostalCode(string postalCode)
        {
            if (string.IsNullOrWhiteSpace(postalCode))
            {
                throw new ExerciseException($"{nameof(postalCode)} can't be null", "101");
            }
            PostalCode = postalCode;
            Update();
        }
        private void Update()
        {
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
