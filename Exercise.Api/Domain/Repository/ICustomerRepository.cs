﻿using Exercise.Api.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Domain.Repository
{
    public interface ICustomerRepository
    {
        Task<List<Customer>> BrowseAsync();
        Task<Customer> GetAsync(Guid customerId);
        Task<Customer> GetAsync(string email);
        Task AddAsync(Guid id, string firstName, string lastName, string email, string street, string postalCode, string city);
        Task UpdateAsync(Guid customerId, string firstName, string lastName, string email, string street, string postalCode, string city);
        Task RemoveAsync(Guid customerId);
    }
}
