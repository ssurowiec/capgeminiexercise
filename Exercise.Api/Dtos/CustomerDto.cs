﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Dtos
{
    public class CustomerDto
    {
        public Guid Id { get; }
        public string FirstName { get; }
        public string LastName { get; }

        public CustomerDto(Guid id, string firstName, string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }
        protected CustomerDto() { }
    }
}
