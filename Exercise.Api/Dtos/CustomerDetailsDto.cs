﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Dtos
{
    public class CustomerDetailsDto
    {
        public Guid Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string City { get; private set; }
        public string  Street { get; private set; }
        public string PostalCode { get; private set; }
        public CustomerDetailsDto(Guid id, string firstName, string lastName, string email,
            string city, string street, string postalCode )
        {
            Email = email;
            City = city;
            PostalCode = postalCode;
            Street = street;
        }
        private CustomerDetailsDto() {}
    }
}
