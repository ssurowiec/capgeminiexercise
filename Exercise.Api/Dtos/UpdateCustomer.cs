﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Dtos
{
    public class UpdateCustomer : CreateCustomer
    {
        [JsonConstructor]
        public UpdateCustomer(string firstName, string lastName, string email, string city, string street, string postalCode) :
            base(firstName, lastName, email, city, street, postalCode)
        {
        }
    }
}
