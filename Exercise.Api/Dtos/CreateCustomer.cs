﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Dtos
{
    public class CreateCustomer
    {
        public Guid Id { get; private set; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Email { get; }
        public string City { get; }
        public string Street { get; }
        public string PostalCode { get; }

        [JsonConstructor]
        public CreateCustomer(string firstName, string lastName, string email,
            string city, string street, string postalCode)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            City = city;
            Street = street;
            PostalCode = postalCode;
        }

        public void SetId(Guid id)
        {
            Id = id;
        }
    }
}
