﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Exercise.Api.Domain.Repository;
using Exercise.Api.Dtos;
using Exercise.Common.Exceptions;

namespace Exercise.Api.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }
        public async Task AddAsync(CreateCustomer command)
        {
            var customer = await _customerRepository.GetAsync(command.Email);
            if(!(customer is null))
            {
                throw new ExerciseException($"User with mail '{command.Email}' already exist", "102");
            }
            await _customerRepository.AddAsync(command.Id, command.FirstName, command.LastName, command.Email,
                command.Street, command.PostalCode, command.City);
        }

        public async Task<IEnumerable<CustomerDto>> BrowseAsync()
            => _mapper.Map<IEnumerable<CustomerDto>>(await _customerRepository.BrowseAsync());

        public Task<CustomerDetailsDto> GetAsync(Guid customerId)
            => _mapper.Map<Task<CustomerDetailsDto>>(_customerRepository.GetAsync(customerId));

        public Task RemoveAsync(Guid customerId)
            => _customerRepository.RemoveAsync(customerId);

        public Task UpdateAsync(UpdateCustomer command)
            => _customerRepository.UpdateAsync(command.Id, command.FirstName, command.LastName,
                command.Email, command.Street, command.PostalCode, command.City);
    }
}
