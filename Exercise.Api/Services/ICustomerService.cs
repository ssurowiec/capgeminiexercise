﻿using Exercise.Api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerDto>> BrowseAsync();
        Task<CustomerDetailsDto> GetAsync(Guid customerId);
        Task AddAsync(CreateCustomer command);
        Task UpdateAsync(UpdateCustomer command);
        Task RemoveAsync(Guid customerId);
    }
}
