﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercise.Api.Dtos;
using Exercise.Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exercise.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        [HttpGet]
        public async Task<IActionResult> BrowseAsync()
            => Ok(await _customerService.BrowseAsync());

        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(Guid id)
        {
            var customer = await _customerService.GetAsync(id);
            if(customer is null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateCustomer command)
        {
            command.SetId(Guid.NewGuid());
             await _customerService.AddAsync(command);
            return Created($"api/Customers/{command.Id}", null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateCustomer command)
        {
            command.SetId(id);
            await _customerService.UpdateAsync(command);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _customerService.RemoveAsync(id);

            return Ok();
        }
    }
}
