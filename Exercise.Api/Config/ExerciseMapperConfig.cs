﻿using AutoMapper;
using Exercise.Api.Domain.Models;
using Exercise.Api.Dtos;
using Exercise.Common.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.Api.Config
{
    public class ExerciseMapperConfig : IAutoMapperConfiguration
    {
        public IMapper GenerateMaps()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Customer, CustomerDto>();
                cfg.CreateMap<Customer, CustomerDetailsDto>()
                    .ForMember(mbr => mbr.City, opt => opt.MapFrom(p => p.Address.City))
                    .ForMember(mbr => mbr.Street, opt => opt.MapFrom(p => p.Address.Street))
                    .ForMember(mbr => mbr.PostalCode, opt => opt.MapFrom(p => p.Address.PostalCode));

            }).CreateMapper();
    }
}
